# TodoList

- [ ] Add an 'Add all ingredients' option
- [ ] Host app on Gitlab Pages
- [ ] Handle ingredients added multiple times
- [ ] Create 'Add recipe' form
- [ ] Create an order history
- [ ] Data in Firebase database
- [ ] Google auth

# Done

- [x] Init app
- [x] Init Todo list in README
- [x] Data in JSON
- [x] Retrieve data in js file
- [x] Expandables tables
- [x] Ingredients in clickable pills
- [x] Populate an array with ingredients on pills click
- [x] Display the array


