import { Grid } from '@material-ui/core'

const ListingRecettes = ({ children }) => {
  return (
    <>
      <Grid container spacing={2}>
        {children}
      </Grid>
    </>
  )
}

export default ListingRecettes
