import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import Language from '@material-ui/icons/Language'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { Typography, Box, Divider, Grid, Chip } from '@material-ui/core'
import ingredients from '../data/ingredients'

const useStyles = makeStyles((theme) => ({
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}))

export default function RecetteCard({
  recette,
  updateIngredientToShoppingList,
}) {
  const classes = useStyles()
  const [expanded, setExpanded] = useState(false)

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  const handleUpdateShoppingList = (id) => {
    const ingredient = ingredients[id]
    ingredient.checked = !ingredient.checked
    updateIngredientToShoppingList(ingredient.titre)
  }

  return (
    <Grid item xs={12} sm={6} lg={3}>
      <Card>
        <CardHeader
          title={recette.titre}
          subheader={<Typography variant='caption'>{recette.type}</Typography>}
        />
        <CardMedia className={classes.media} image={recette.image} />
        <CardActions disableSpacing>
          <Typography variant='caption'>
            {'Commandé le : ' + recette.ordered}
          </Typography>
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label='show more'
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout='auto' unmountOnExit>
          <CardContent>
            {recette.ingredients_id.map((id, key) => (
              <Box mx={1} key={key} component='span'>
                <Chip
                  label={ingredients[id].titre}
                  color={ingredients[id].checked ? 'primary' : 'default'}
                  onClick={() => handleUpdateShoppingList(id)}
                />
              </Box>
            ))}
            <Box my={2}>
              <Divider />
            </Box>
            <a
              className='source-link'
              href={recette.source}
              title={recette.titre}
            >
              <IconButton>
                <Language />
              </IconButton>
              Lien de la recette
            </a>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  )
}
