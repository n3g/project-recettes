import React from 'react'
import { Box, Paper } from '@material-ui/core'

const IngredientAddedBar = ({ ingredients }) => {
  return (
    <div>
      {ingredients.length > 0 && (
        <>
          <Paper>
            <Box p={3} m={3}>
              <h1>
                {ingredients.length}
                {ingredients.length <= 1 ? ' Ingrédient' : ' Ingrédients'}
              </h1>
              <ul>
                {ingredients.map((ingredient, key) => (
                  <li key={key}>{ingredient}</li>
                ))}
              </ul>
            </Box>
          </Paper>
        </>
      )}
    </div>
  )
}

export default IngredientAddedBar
