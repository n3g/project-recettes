let ingredients = [
  {
    id: 0,
    titre: 'Pain de mie',
    category: 'Produit frais',
    checked: false,
  },
  {
    id: 1,
    titre: 'Jambon',
    category: 'Produit frais',
    checked: false,
  },
  {
    id: 2,
    titre: 'Fromage',
    category: 'Produit frais',
    checked: false,
  },
  {
    id: 3,
    titre: 'Pâtes',
    category: 'Produit sec',
    checked: false,
  },
  {
    id: 4,
    titre: 'Sauce tomate',
    category: 'Produit sec',
    checked: false,
  },
]

export default ingredients
