let recettes = [
  {
    titre: 'Croque-monsieur',
    image:
      'https://assets.afcdn.com/recipe/20170112/28965_w1024h768c1cx1500cy1000.webp',
    ingredients_id: [0, 1, 2],
    type: 'Plat principal',
    ordered: '15 mai 2019',
    source: 'https://www.recettes.fr',
  },
  {
    titre: 'Pâte bolognaise',
    image:
      'https://assets.afcdn.com/recipe/20160419/14652_w1024h576c1cx2420cy1872.webp',
    ingredients_id: [2, 3, 4],
    type: 'Plat principal',
    ordered: '30 mai 2020',
    source: 'https://www.recettes.fr',
  },
]

export default recettes
