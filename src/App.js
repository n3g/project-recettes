import { useState } from 'react'
import IngredientAddedBar from './components/IngredientAddedBar'
import ListingRecettes from './components/ListingRecettes'
import RecetteCard from './components/RecetteCard'
import recettes from './data/recettes'
import './style.scss'

function App() {
  const [shoppingList, setShoppingList] = useState([])

  const updateIngredientToShoppingList = (id) => {
    if (shoppingList.includes(id)) {
      // remove
      const i = shoppingList.indexOf(id)
      const tempArr = [...shoppingList]
      tempArr.splice(i, 1)
      setShoppingList(tempArr)
      // add
    } else {
      setShoppingList([...shoppingList, id])
    }
  }

  return (
    <>
      <ListingRecettes>
        {recettes.map((recette, key) => (
          <RecetteCard
            key={key}
            recette={recette}
            updateIngredientToShoppingList={updateIngredientToShoppingList}
          />
        ))}
      </ListingRecettes>
      <IngredientAddedBar ingredients={shoppingList} />
    </>
  )
}

export default App
